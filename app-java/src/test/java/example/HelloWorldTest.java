package example;

import org.junit.Test;

public class HelloWorldTest {


  @Test
  public void coveredByUnitTest() {
    System.out.println("coveredByUnitTest1");
    System.out.println("coveredByUnitTest2");
  }

  @Test
  public void coveredByIntegrationTest() {
    System.out.println("coveredByIntegrationTest1");
    System.out.println("coveredByIntegrationTest2");
    System.out.println("coveredByIntegrationTest3");
	//
  }

  @Test
  public void notCovered() {
    System.out.println("notCovered");
  }

}
